import type { Route } from "./Router/router.service";
import { importBoard } from "./Services/board.service";
import Dashboard from './Views/Dashboard.svelte';
import WhiteBoard from './Views/Whiteboard.svelte';

const Routes: Route[] = [
    { path: '/', component: Dashboard },
    { path: '/board/:id', component: WhiteBoard },
    <any>{ path: '/import', execute: importBoard },

];

export default Routes;