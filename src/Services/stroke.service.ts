
import type FeedStore from 'orbit-db-feedstore';
import { BehaviorSubject, combineLatest, fromEvent, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { get, writable } from 'svelte/store';
import { startNode, stopNode } from '../mfkip.config';

let node: MFKIP;
let dbResolver;
let db$ = new Promise<FeedStore<Shape>>(res => dbResolver = res);
let undoStack = [];

const remoteStrokes = new BehaviorSubject<Shape[]>([]);
const destroy$ = new Subject();

export const pages$ = new BehaviorSubject(0);
export const currentPage$ = new BehaviorSubject(0);
export const canUndo$ = new BehaviorSubject(false);
export const authorId = localStorage.authorId || (() => {
    const id = Math.random().toString(36);
    localStorage.authorId = id;
    return id;
})();

export const strokeWidth = writable(1);

export function start(id: string) {
    db$ = new Promise<FeedStore<Shape>>(res => dbResolver = res);
    remoteStrokes.next([]);
    currentPage$.next(0);
    canUndo$.next(false);
    pages$.next(0);
    undoStack = [];

    startNode(id)
        .pipe(
            filter(Boolean),
        ).subscribe(async node => {
            const db = await node.openDb('strokes', <any>'feed');
            dbResolver(db);
            
            combineLatest([
                fromEvent(db.events, 'mfkip.change'),
                currentPage$,
            ]).pipe(
                takeUntil(destroy$)
            ).subscribe(([_, currentPage]) => {
                const currPage = currentPage;
                const shapes = db.all.map(m => m.payload.value);

                pages$.next(shapes.reduce((maxPage, msg) => Math.max(maxPage, msg.page || 0), 0));
                remoteStrokes.next(
                    shapes
                        .filter((shape: Shape) => shape.page === currPage)
                );
            })
        });
}

export function setPage(page: number) {
    currentPage$.next(page);
}

export async function stop() {
    destroy$.next(null);
    stopNode();
}

export async function publishStroke(stroke: string, color: string) {
    const db = await db$;
    const strokeId = await db.add({
        page: currentPage$.getValue(),
        path: stroke,
        color: color || 'black',
        author: authorId,
        id: Math.random().toString(36),
        strokeWidth: get(strokeWidth)
    });
    
    undoStack.push(strokeId);
    canUndo$.next(true);
}

export async function undoStroke() {
    if (!undoStack.length) return;

    const strokeId = undoStack.pop();
    canUndo$.next(undoStack.length > 0);

    const db = await db$;
    await db.remove(strokeId);
}

export function shapes() { return remoteStrokes.asObservable(); };

export interface Shape {
    id: string;
    page: number;
    path: string;
    color: string;
    author: string;
    strokeWidth?: number;
}
