import { writable, Readable } from "svelte/store";
import { getActivatedRoute, navigate } from "../Router/router.service";

const _boards: Boards = JSON.parse(localStorage.scribbleBoards || '[]');
const _boards$ = writable<Boards>(_boards);

export const boards = <Readable<Boards>>_boards$;
export async function addBoard(board: Board) {
    _boards.push(board);
    _boards$.set(_boards);

    localStorage.scribbleBoards = JSON.stringify(_boards);
}

const urlFields = ['id', 'name', 'swarmKey', 'hasPassword'];
export async function importBoard() {
    const r = getActivatedRoute();
    await addBoard(
        { ...<any>Object.fromEntries(urlFields.map(k => [k, r.params[k]])), createdAt: Date.now() }
    );
    navigate('/board/' + r.params.id);
}

export function exportBoardUrl(board: Board) {
    const params = new URLSearchParams(Object.fromEntries(urlFields.map(k => [k, board[k]])));
    if (!board.hasPassword) {
        params.delete('hasPassword');
    }
    return `${location.origin}${location.pathname}#/import?${params.toString()}`;
}

export type Board = {
    id: string,
    name: string,
    createdAt: number,
    swarmKey: string,
    hasPassword?: boolean,
}

export type Boards = Board[];