import Navigo, { Match } from 'navigo';

export type ActivatedRoute = Match;
export type SvelteComponent = any;
export interface Route {
    path: string;
    component: SvelteComponent;
    overlay?: SvelteComponent;
    pre?: () => boolean;
    execute?: (ar: ActivatedRoute) => void;
}
export type SetView = (component: SvelteComponent, overlay: SvelteComponent) => void;

const router = new Navigo('/', { hash: true });
router.notFound(() => {
    console.log('Route not found');
    router.navigate('/');
});

let activatedRoute = null;
export function withRoutes(routes: Route[], setView: SetView) {
    routes.forEach(route => {
        router.on(route.path, (match) => {
            activatedRoute = match;

            if (route.execute) {
                route.execute(activatedRoute);
            } else {
                if (route.pre) {
                    if (route.pre()) {
                        setView(route.component, route.overlay);
                    }
                } else {
                    setView(route.component, route.overlay);
                }
            }
        });
    });

    router.resolve();
}

export function getActivatedRoute() {
    return activatedRoute;
}

export function navigate(url) {
    router.navigate(url);
}

export function back() {
    window.history.back();
}
