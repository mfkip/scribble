import { fromEvent, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { get, writable } from "svelte/store";

export const scale = writable(.25);
export class WhiteboardNavigator {

    offsetX = writable(0);
    offsetY = writable(0);
    panning = writable(false);

    private svgWrapper = this.svgEl.parentElement;
    mc = new Hammer.Manager(this.svgWrapper);

    private destroy$ = new Subject();

    constructor(private svgEl: SVGElement) {

        const sensitivity = 0.0001;
        fromEvent(svgEl, 'mousewheel')
            .pipe(takeUntil(this.destroy$))
            .subscribe((e: WheelEvent) => {
                e.preventDefault();
                // Ensure zoom speed scales with zoom, or else zooming in take forever
                scale.set(Math.min(
                    20,
                    Math.max(0.05, get(scale) + e.deltaY * sensitivity * -(get(scale) * 10))
                ));
            });

        const Pan = new Hammer.Pan({
            pointers: 1,
            threshold: 0,
        });
        const Pinch = new Hammer.Pinch();
        this.mc.add(Pinch);
        this.mc.add(Pan);

        let scaleBefore: number;
        this.mc.on("pinchstart", (e) => {
            scaleBefore = get(scale);
            this.panning.set(true);
        });

        this.mc.on("pinchmove", (e) => {
            // Handle 2Finger Pan
            const pointerEvent = e.changedPointers[0];
            this.offsetX.set(
                get(this.offsetX) + pointerEvent.movementX * (1 / (get(scale) * window.devicePixelRatio))
            );
            this.offsetY.set(
                get(this.offsetY) + pointerEvent.movementY * (1 / (get(scale) * window.devicePixelRatio))
            );

            // Handle Pinch
            scale.set(Math.min(20, Math.max(0.05, scaleBefore * e.scale)));
        });

        this.mc.on('panstart', e => {
            const pointerEvent = e.changedPointers[0];
            if (pointerEvent.ctrlKey || get(this.panning)) {
                e.preventDefault();
                this.panning.set(true);
            }
        });

        this.mc.on("panmove", (e) => {
            const pointerEvent = e.changedPointers[0];

            if (pointerEvent.ctrlKey || get(this.panning)) {
                e.preventDefault();
                this.offsetX.set(
                    get(this.offsetX) + pointerEvent.movementX * (1 / get(scale))
                );
                this.offsetY.set(
                    get(this.offsetY) + pointerEvent.movementY * (1 / get(scale))
                );
            }
        });

        this.mc.on('panend', e => {
            if (get(this.panning)) {
                e.preventDefault();
                this.panning.set(false);
            }
        })

        fromEvent(window, 'keydown')
            .pipe(takeUntil(this.destroy$))
            .subscribe((e: KeyboardEvent) => this.onSpaceDown(e));
        fromEvent(window, 'keyup')
            .pipe(takeUntil(this.destroy$))
            .subscribe((e: KeyboardEvent) => this.onSpaceUp(e));
    }

    destroy() {
        this.destroy$.next(null);
        this.destroy$.complete();
        this.mc.destroy();
    }

    private onSpaceDown(e: KeyboardEvent) {
        if (!get(this.panning) && e.key === " ") {
            this.panning.set(true);
        }
    }

    private onSpaceUp(e: KeyboardEvent) {
        if (e.key === " ") {
            this.panning.set(false);
        }
    }
}