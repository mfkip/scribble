import { Path, setup } from 'paper';
import { BehaviorSubject, combineLatest, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { get, Readable } from "svelte/store";
import { authorId, publishStroke, Shape, shapes } from "../../Services/stroke.service";
import { scale, WhiteboardNavigator } from "./whiteboard.navigator";

export class ShapesModel {

    private local: BehaviorSubject<paper.Path>;
    private remote = shapes();
    paths: Observable<Shape[]>;

    private color: Readable<string>

    constructor() {
        // paper.js absolutely blows...
        (setup as any)();
        this.local = new BehaviorSubject<paper.Path>(new Path());
        this.paths = combineLatest([
            this.remote.pipe(),
            this.local.pipe(
                map((path: paper.Path) => ({
                    path: (path.exportSVG() as SVGElement).attributes.getNamedItem("d").value,
                    color: this.color ? get(this.color) : 'black',
                    author: authorId,
                })),
            )
        ]).pipe(
            map(([orbitPaths, localpath]: [Shape[], Shape]) => [
                ...orbitPaths,
                localpath,
            ])
        )
    }

    attach(wbNavigator: WhiteboardNavigator, color: Readable<string>) {
        this.color = color;
        wbNavigator.mc.on('panstart', e => {
            if (get(wbNavigator.panning)) {
                return;
            }
            const pointerEvent = e.changedPointers[0];

            this.local
                .getValue()
                .add([pointerEvent.offsetX, pointerEvent.offsetY]);
            this.local.next(this.local.getValue());
        });

        wbNavigator.mc.on("panmove", (e) => {
            if (get(wbNavigator.panning)) {
                return;
            }
            const pointerEvent = e.changedPointers[0];

            if (this.local.getValue().segments.length) {
                this.local
                    .getValue()
                    .add([pointerEvent.offsetX, pointerEvent.offsetY]);
                this.local.next(this.local.getValue());
            }
        });

        wbNavigator.mc.on("panend", (e) => {
            let segments = this.local.getValue().segments;

            if (segments.length > 1) {
                console.log(`before simplification: ${segments.length}`);
                this.local.getValue().simplify(10 * (1 / get(scale)));
                segments = this.local.getValue().segments;
                console.log(`after simplification: ${segments.length}`);
                const stroke = (<SVGElement>(
                    this.local.getValue().exportSVG()
                )).attributes.getNamedItem("d").value;
                    
                publishStroke(stroke, get(color));
            }

            this.local.next(new Path());
        });
    }
}