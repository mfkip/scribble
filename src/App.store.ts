import { readable } from 'svelte/store';

export type InstallPrompt = Event & {
    prompt: () => Promise<void>
}

export default {
    installPrompt: readable(null, set => {
        window.addEventListener('beforeinstallprompt', (e: InstallPrompt) => set({
            prompt: async () => {
                await e.prompt();
                set(null);
            }
        }));
    }),
}