/// <reference path="../node_modules/@mfkip/web-lib/dist/index.d.ts" />
import 'hammerjs';
import App from './App/App.svelte';

declare global {
	const Hammer: any;
}

const app = new App({
	target: document.body,
});

function checkForSwSupport() {
	if (!('serviceWorker' in navigator)) {
		throw new Error('No Service Worker support!')
	}
}

async function registerServiceWorker() {
	return await navigator.serviceWorker.register('./bundle.sw.js', { scope: './' });
}

(async () => {
	if (!window.dev) {
		checkForSwSupport();
		await registerServiceWorker();
	}
})()

window.addEventListener('appinstalled', e => {
	console.log('window.appinstalled', e);
});

export default app;