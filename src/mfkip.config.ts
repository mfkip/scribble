import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { boards as boards$ } from './Services/board.service';

let node: MFKIP;

export const authorId = localStorage.authorId || (() => {
    const id = Math.random().toString(36);
    localStorage.authorId = id;
    return id;
})();

const destroy$ = new Subject();

export function startNode<T>(id: string): Observable<MFKIP> {
    const $node = new BehaviorSubject<MFKIP>(null);
    const boardsSubscription = boards$.subscribe(async boards => {
        const board = boards.find(b => b.id === id);

        if (!board) return;

        node = (window as any).mfkip = new MFKIP({
            encodedKey: board.swarmKey,
            listenAddresses: [
                '/dns4/guarded-anchorage-63004.herokuapp.com//tcp/443/wss/p2p-webrtc-star'
            ],
            appId: 'scribble',
            minConnections: 5,
            maxConnections: 20,
            partitions: [board.id],
        });

        node.start();

        destroy$.pipe(
            first(),
        ).subscribe(() => {
            boardsSubscription();
            node.stop();
        });

        $node.next(node);
    });

    return $node.asObservable();
}

export function stopNode() {
    destroy$.next(null);
}